// CONFIG DATEI

// Allgemeine einstellungen
#define SWITCH_DURATION 20 // milliseconds
#define ON_DURATION 2      // minutes
#define TIMEZONE +1

// Wifi Einstellungen
#define WIFI_SSID "Alex’s iPhone"
#define WIFI_PASSWD "Larentis2001"

// Aufbau vom Schedule
//
//
// erste klammer ist eine Liste. In dieser bedinden sich die
// einzelnen Schedule eintraege. Ein Schedule besteht aus
// eimen Aktor und einer Zeit
//
//  SCHEDULE       - { {SCHEDULE ENTRY}, ... }
//  SCHEDULE ENTRY - { AKTOR, ZEIT }
//  AKTOR          - { ID:int, ON_GPIO:int, OFF_GPIO:int }
//  ZEIT           - { MIN:int, STUNDE:int }
//
// Komplett mit platzhaltern:
// { {{ID, AN_GPIO, AUS_GPIO}, {STUNDE, MINUTE}}, ...}
//
// ESCAPE CHAR NET VERGESSEN '\'
#define SCHEDULE                      \
    {                                 \
        {{1, 15,  2}, {12, 16}, 1},    \
        {{2, 15,  2}, {12, 17}, 1},    \
        {{3,  0,  4}, {12, 18}, 1},    \
        {{4,  0,  4}, {12, 19}, 1},    \
        {{5, 16, 17}, {12, 20}, 1},   \
        {{6, 18, 19}, {12, 21}, 1}    \
    }