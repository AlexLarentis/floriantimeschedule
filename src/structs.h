#ifndef structs_f
#define structs_f

struct t_actor
{
    int id;
    int gpio_on;
    int gpio_off;
};

struct tf_time
{
    int hour;
    int minute;
};

bool operator<=(const tf_time &x, const tf_time &y)
{
    int time_1_min = x.minute + (x.hour * 60);
    int time_2_min = y.minute + (y.hour * 60);

    return time_1_min <= time_2_min;
}

bool operator>=(const tf_time &x, const tf_time &y)
{
    int time_1_min = x.minute + (x.hour * 60);
    int time_2_min = y.minute + (y.hour * 60);

    return time_1_min >= time_2_min;
}

struct t_actor_time
{
    t_actor actor;
    tf_time time;
    int duration;
};
#endif