#include <NTPClient.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include "structs.h"
#include "config.h"

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

char ptrTaskList[250];

const std::vector<t_actor_time> schedule = SCHEDULE;

void setup()
{
  Serial.begin(115200);
  WiFi.begin(WIFI_SSID, WIFI_PASSWD);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  timeClient.begin();
  timeClient.setTimeOffset(TIMEZONE * 3600);
  Serial.println();
}

bool compare_times(tf_time start, tf_time current)
{
  return start <= current && start >= current;
}

void schedule_task(void *data)
{
  t_actor_time actor = *(t_actor_time *)data;

  Serial.print("Turining on Actor: ");
  Serial.println(actor.actor.id);
  

  /* Umrechnung von Minuten in Millisekunden */
  const TickType_t xDelay = actor.duration * 60000 / portTICK_PERIOD_MS;
  vTaskDelay(xDelay);
  Serial.print("Turining off Actor: ");
  Serial.println(actor.actor.id);

  // deleting task
  vTaskDelete(NULL);
}

int last_computed_min = -1;
void loop()
{

  timeClient.update();
  if (last_computed_min != timeClient.getMinutes())
  {
    struct tf_time current = {timeClient.getHours(),
                              timeClient.getMinutes()};

    for (size_t i = 0; i < schedule.size(); i++)
    {
      if (compare_times(schedule.at(i).time, current))
      {
        t_actor_time actor_time = schedule.at(i);
        xTaskCreate(&schedule_task, "schedule_task", 1024, &actor_time, 5, NULL);
      }
    }

    last_computed_min = current.minute;
  }
  delay(300);
}